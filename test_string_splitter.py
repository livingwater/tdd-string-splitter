import string_splitter

def test_split_empty_string_result_empty_array():
    # arrange
    stringToSplit = ""
    expResult = []
    result = None
    cut = string_splitter.TagManipulator()
 
    # act
    result = cut.parse_string(stringToSplit)
 
    # assert
    assert result == expResult

def test_split_comma_empty_string_result_empty_array():
    # arrange
    stringToSplit = ","
    regex = ",\\ *"
    expResult = []
    result = None
    cut = string_splitter.TagManipulator()
 
    # act
    result = cut.parse_string(stringToSplit, regex)
 
    # assert
    assert result == expResult

def test_split_one_string_result_array_of_one():
    # arrange
    stringToSplit = "java"
    regex = ",\\ *"
    expResult = ["java"]
    result = None
    cut = string_splitter.TagManipulator()
 
    # act
    result = cut.parse_string(stringToSplit, regex)
 
    # assert
    assert result == expResult

def test_split_two_string_result_array_of_two():
    # arrange
    stringToSplit = "java, code"
    regex = ",\\ *"
    expResult = ["java", "code"]
    result = None
    cut = string_splitter.TagManipulator()
 
    # act
    result = cut.parse_string(stringToSplit, regex)
 
    # assert
    assert result == expResult

def test_split_three_string_result_array_of_three():
    # arrange
    stringToSplit = "java, code, testing"
    regex = ",\\ *"
    expResult = ["java", "code", "testing"]
    result = None
    cut = string_splitter.TagManipulator()
 
    # act
    result = cut.parse_string(stringToSplit, regex)
 
    # assert
    assert result == expResult

def test_edge_split_one_string_comma_result_array_of_one():
    # arrange
    stringToSplit = "java,"
    regex = ",\\ *"
    expResult = ["java"]
    result = None
    cut = string_splitter.TagManipulator()
 
    # act
    result = cut.parse_string(stringToSplit, regex)
 
    # assert
    assert result == expResult

def test_edge_split_one_comma_string_result_array_of_one():
    # arrange
    stringToSplit = ",java"
    regex = ",\\ *"
    expResult = ["java"]
    result = None
    cut = string_splitter.TagManipulator()
 
    # act
    result = cut.parse_string(stringToSplit, regex)
 
    # assert
    assert result == expResult

def test_edge_split_one_comma_string_comma_result_array_of_one():
    # arrange
    stringToSplit = ",java,"
    regex = ",\\ *"
    expResult = ["java"]
    result = None
    cut = string_splitter.TagManipulator()
 
    # act
    result = cut.parse_string(stringToSplit, regex)
 
    # assert
    assert result == expResult

def test_edge_split_one_string_many_comma_result_array_of_one():
    # arrange
    stringToSplit = "java,,,"
    regex = ",\\ *"
    expResult = ["java"]
    result = None
    cut = string_splitter.TagManipulator()
 
    # act
    result = cut.parse_string(stringToSplit, regex)
 
    # assert
    assert result == expResult

def test_edge_split_many_comma_string_result_array_of_one():
    # arrange
    stringToSplit = ",,,java"
    regex = ",\\ *"
    expResult = ["java"]
    result = None
    cut = string_splitter.TagManipulator()
 
    # act
    result = cut.parse_string(stringToSplit, regex)
 
    # assert
    assert result == expResult

def test_edge_split_one_string_bad_chars_result_array_of_one():
    # arrange
    stringToSplit = "java\\//"
    regex = ",\\ *"
    expResult = ["java"]
    result = None
    cut = string_splitter.TagManipulator()
 
    # act
    result = cut.parse_string(stringToSplit, regex)
 
    # assert
    assert result == expResult