import re

class TagManipulator:
    def parse_string(self, tags, regex=""):
        result = []
    
        tempResult = re.split(regex, tags.strip().replace("\\", "").replace("//", ""))
    
        for tag in tempResult:
            if( len( tag ) > 0 ):
                result.append(tag)
    
        return result